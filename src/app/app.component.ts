import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { tap } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ConfigService } from '@rxap/config';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ]
})
export class AppComponent implements OnInit {

  public token$ = new ReplaySubject(1);

  public messages: Array<{ notification: { title: string, body: string } }> = [];

  constructor(
    private readonly message: AngularFireMessaging,
    private readonly snackBar: MatSnackBar,
    private readonly cdr: ChangeDetectorRef,
    private readonly config: ConfigService,
  ) {
  }

  public get firebaseConsoleUrl(): string {
    return `https://console.firebase.google.com/project/${this.config.get('firebase.options.projectId')}/notification/compose`;
  }

  public ngOnInit(): void {
    this.message.tokenChanges.pipe(
      tap(token => console.log('token: ' + token)),
    ).subscribe(token => this.token$.next(token));
    this.message.messages
      .pipe(
        tap((message: any) => this.showNotification(message.notification.title, message.notification.body)),
        tap(message => this.messages.push(message)),
        tap(() => this.cdr.detectChanges()),
      )
      .subscribe((message) => {
        console.log(message);
      });
  }

  public requestPermission(): void {
    this.message.requestPermission
      .subscribe(
        () => {
          console.log('Permission granted!');
        },
        (error) => {
          console.error(error);
        },
      );
  }

  public copied(): void {
    this.snackBar.open('Copied to clipboard', 'OK', { duration: 2500 });
  }

  public showNotification(title: string, body: string): void {
    // Let's check if the browser supports notifications
    if (!('Notification' in window)) {
      alert('This browser does not support desktop notification');
    }

    // Let's check whether notification permissions have already been granted
    else if (Notification.permission === 'granted') {
      // If it's okay let's create a notification
      new Notification(title, { body });
    } else {
      console.warn('Notification permission is not granted');
    }
  }

  public openInNewWindow(): void {
    window.open(this.firebaseConsoleUrl, 'name', 'width=800,height=1000');
  }
}
