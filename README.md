RxAP Firebase Cloud Messaging Demo
===

# Usage

1. Pull the docker image `docker pull registry.gitlab.com/rxap/demo/firebase-cloud-messaging`.
2. Start the docker container `docker run --rm -p 8888:80 registry.gitlab.com/rxap/demo/firebase-cloud-messaging`.
3. Open the browser [http://localhost:8888](http://localhost:8888)

## Custom firebase config

To provide a custom firebase config the files `config.json` and `firebase-init.js` in the container must be overwritten:

```bash
docker run \
  --rm \
  -v "$PWD/config.json:/usr/share/nginx/html/config.json" \
  -v "$PWD/firebase-init.js:/usr/share/nginx/html/firebase-init.js" \
  -p 8888:80 \
  registry.gitlab.com/rxap/demo/firebase-cloud-messaging
```

**Example `firebase-init.js` file**

```javascript
firebase.initializeApp({
  "apiKey": "AIzaSyAznE_K7UUjjGiE8wEr7grBQAcrYrE0BIA",
  "authDomain": "cloud-messaging-testing-15695.firebaseapp.com",
  "projectId": "cloud-messaging-testing-15695",
  "storageBucket": "cloud-messaging-testing-15695.appspot.com",
  "messagingSenderId": "3902961473",
  "appId": "1:3902961473:web:b31334b7594037236d53c0"
});
```

**Example `config.json` file**

```json
{
  "firebase": {
    "options": {
      "apiKey": "AIzaSyAznE_K7UUjjGiE8wEr7grBQAcrYrE0BIA",
      "authDomain": "cloud-messaging-testing-15695.firebaseapp.com",
      "projectId": "cloud-messaging-testing-15695",
      "storageBucket": "cloud-messaging-testing-15695.appspot.com",
      "messagingSenderId": "3902961473",
      "appId": "1:3902961473:web:b31334b7594037236d53c0"
    }
  }
}
```

## Send Notifications

1. Open the demo app [http://localhost:8888](http://localhost:8888).
2. Click on the button **Request Permission** and accept the popup.
3. Wait until the **FCM Token** is loaded and copy the token to your clipboard.
4. Click on the button **Open Firebase Console**.
5. Enter an *Notification title* and *Notification text* and click on **Send test message**.
6. Enter the **FCM Token** in the input *Add an FCM registration token* and click on the plus button.
7. Click on the **Test** button and switch back to the demo app.
8. Wait until the message is end to the browser.
9. (optional) Open the message details with a click on the expand panel **Show raw data**.

> **IMPORTANT**: The demo app tab muss be the focus tab in the browser window.
> If the app is *not* the focus tab in the browser window the notification is
> send as *background notification* and will not be display below the **Received Foreground Notifications:** title.
