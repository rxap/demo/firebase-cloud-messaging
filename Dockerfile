FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY ./dist/demo /usr/share/nginx/html
